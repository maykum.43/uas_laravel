<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminModel extends Model
{
    protected $table = 'tb_admin';
    protected $fillable = [
        'id_admin','nama','no_tlp','alamat','jenis_kelamin','is_active'
    ];

    public function hasManyProduct(){
        return $this->hasMany(KendaraanModel::class,'id_admin','id_admin');
    }
}
