<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminModel;

class AdminController extends Controller
{
    public function index(){
        $data = AdminModel::all()
                ->where('is_active','1');
        return view('admin.admin.index', compact('data'));
    }

    public function createData(){
        return view('admin.admin.tambah');
    }

    public function postData(Request $request, AdminModel $AdminModel){

        $simpan = $AdminModel->create([
            'nama'              => $request->nama,
            'no_tlp'            => $request->no_tlp,
            'alamat'            => $request->alamat,
            'jenis_kelamin'     => $request->jenis_kelamin,
            'is_active'         => 1,
        ]);

        if(!$simpan->exists){
            return redirect()->route('tampil_admin')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_admin')->with('success','data berhasil disimpan');
    }

    public function editData($id){
        $data = AdminModel::where('id_admin',$id)->first();
        return view('admin.admin.edit', compact('data'));
    }

    public function updateData($id,AdminModel $AdminModel, Request $request){
        
        $simpan = $AdminModel->where('id_admin',$id)->update([
            'nama'              => $request->nama,
            'no_tlp'            => $request->no_tlp,
            'alamat'            => $request->alamat,
            'jenis_kelamin'     => $request->jenis_kelamin,
            'is_active'         => $request->is_active,
        ]);
        if(!$simpan){
            return redirect()->route('tampil_admin')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_admin')->with('success','data berhasil di update');
    }

    public function softDelete($id, AdminModel $AdminModel){

        $simpan = $AdminModel->where('id_admin',$id)->update([
            'is_active' => '0',
        ]);

        if(!$simpan){
            return redirect()->route('tampil_admin')->with('error','data gagal di dihapus');
        }

        return redirect()->route('tampil_admin')->with('success','data berhasil di hapus');
    }
}
