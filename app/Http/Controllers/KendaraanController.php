<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KendaraanModel;
use PDF;

class KendaraanController extends Controller
{
        public function index(){
                $data = KendaraanModel::all()
                        ->where('is_active','1');
                return view('admin.kendaraan.index', compact('data'));
        }

        public function daftar_kendaraan(){
                $data = KendaraanModel::all()
                        ->where('is_active','1');
                return view('admin.kendaraan.daftar_kendaraan', compact('data'));

                $cek_motor = DB::select("SELECT count(*) from tb_kendaraan WHERE jenis_kendaraan ='Motor'");
                        return view('admin.kendaraan.daftar_kendaraan',compact('data'));
        }

        public function postData(Request $request, KendaraanModel $KendaraanModel){

                $simpan = $KendaraanModel->create([
                        'id_admin'              => $request->id_admin,
                        'plat_no'               => $request->plat_no,
                        'jenis_kendaraan'       => $request->input('jenken'),
                        'merek'                 => $request->merek,
                        'jam_masuk'             => now(),
                        'hitung_jam_masuk'      == 'jam_masuk',
                        'is_active'             => 1,
                ]);
        
                if(!$simpan->exists){
                    return redirect()->route('tampil_kendaraan')->with('error','data gagal disimpan');
                }
        
                return redirect()->route('tampil_kendaraan')->with('success','data berhasil disimpan');
            }

            public function search( Request $request ,KendaraanModel $KendaraanModel){
                $datacari = KendaraanModel::when($request->cari, function ($query) use ($request){
                    $query->where('id_parkir', 'like', "%{$request->cari}%")
                    ->orWhere('plat_no', 'like', "%{$request->cari}%");
                })->get();
                // dd($serch);
        
                return view('admin.kendaraan.cari_kendaraan',['datacari'=>$datacari]);
            }

            public function cetak_pdf()
            {
                    $kendaraan = KendaraanModel::all();
         
                    $pdf = PDF::loadview('cetak_pdf',['kendaraan'=>$kendaraan]);
                    return $pdf->stream();
            }
        

        
}
