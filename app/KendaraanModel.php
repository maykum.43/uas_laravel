<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class KendaraanModel extends Model
{
    protected $table = 'tb_kendaraan';
    protected $fillable = [
        'id_parkir','id_admin','plat_no','jenis_kendaraan','merek','jam_masuk','hitung_jam_masuk','is_active'
    ];

    public function haveAdmin(){
        return $this->belongTo(AdminModel::class,'id_admin','id_admin');
    }
}
