<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login','AuthController@index')->name('login_form');
Route::post('/sendLogin','AuthController@sendLoginRequest')->name('login_action');
Route::get('/logout','AuthController@logout')->name('logout_action');

Route::get('home','KendaraanController@index')->name('tampil_kendaraan');
// Route::get('kendaraan/create','KendaraanController@createData')->name('create_kendaraan');
Route::post('kendaraan/post','KendaraanController@postData')->name('post_kendaraan');

Route::get('daftar_kendaraan/post','KendaraanController@daftar_kendaraan')->name('daftar_kendaraan');

Route::get('daftar_admin/post','AdminController@index')->name('tampil_admin');
Route::get('admin/create','AdminController@createData')->name('create_admin');
Route::post('admin/post','AdminController@postData')->name('post_admin');

Route::get('admin/edit/{id}','AdminController@editData')->name('edit_data');
Route::post('admin/update/{id}','AdminController@updateData')->name('update_data');

Route::get('/admin/hapus/{id}', 'AdminController@softDelete')->name('delete_data');

Route::get('/parkir_keluar','KendaraanController@search')->name('search');