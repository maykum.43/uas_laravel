<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
                'name'   => 'admin',
                'email'  => 'admin@email.com',
                'password' =>  bcrypt('admin123'),
                'created_at'    => now(),
            ),
            array(
                'name'   => 'fikri',
                'email'  => 'fikri@email.com',
                'password' =>  bcrypt('fikri'),
                'created_at'    => now(),
            ),
        ));
    }
}
