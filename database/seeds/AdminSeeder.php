<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_admin')->insert(array(
            array(
                'id_admin'          => '101',
                'nama'              => 'Fikri Maulana',
                'no_tlp'            => '081234567890',
                'alamat'            => 'Ciwidey',
                'jenis_kelamin'     => 'laki-laki',
                'is_active'         => 1,
                'created_at'        => now(),
            ),
            array(
                'id_admin'          => '102',
                'nama'              => 'M. Fahmi Gunawan',
                'no_tlp'            => '081234567890',
                'alamat'            => 'Majalaya',
                'jenis_kelamin'     => 'laki-laki',
                'is_active'         => 1,
                'created_at'        => now(),
            ),
        ));
    }
}
