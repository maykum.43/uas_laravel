<?php

use Illuminate\Database\Seeder;

class KendaraanMasukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_kendaraan')->insert(array(
            array(
                'id_parkir'         => '101',
                'id_admin'          => '101',
                'plat_no'           => 'D2617DXC',
                'jenis_kendaraan'   =>'Motor',
                'merek'             =>'yamaha',
                'jam_masuk'         => now(),
                'is_active'         => 1,
                'created_at'        => now(),
            ),
            array(
                'id_parkir'         => '102',
                'id_admin'          => '101',
                'plat_no'           => 'D2117UIC',
                'jenis_kendaraan'   =>'Mobil',
                'merek'             =>'Honda',
                'jam_masuk'         => now(),
                'is_active'         => 1,
                'created_at'        => now(),
            ),
            array(
                'id_parkir'         => '103',
                'id_admin'          => '101',
                'plat_no'           => 'B3627IO',
                'jenis_kendaraan'   =>'Lainya',
                'merek'             =>'Lainya',
                'jam_masuk'         => now(),
                'is_active'         => 1,
                'created_at'        => now(),
            ),
        ));
    }
}
