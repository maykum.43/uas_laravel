<!DOCTYPE html>
<html>
<head>
	<title>Struk Parkir</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Struk Parkir</h4>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>ID Parkir</th>
				<th>Plat No.</th>
				<th>Jam Masuk</th>
				<th>Jam Keluar</th>
				<th>Bayar</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($datacari as $row)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$row->id_parkir}}</td>
                                    <td>{{$row->plat_no}}</td>
                                    <td>{{$row->jenis_kendaraan}}</td>
                                    <td>{{$row->merek}}</td>
                                    <td>{{$row->jam_masuk}}</td>
                                    </td>
                                </tr>
                                @endforeach
		</tbody>
	</table>

</body>
</html>