@extends('admin.layouts.master')
@section('content')

@php
date_default_timezone_set("Asia/Jakarta");

$i=1;

@endphp

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Daftar Kendaraan Yang dicari</h4> 
                </div>
                <div class="card-body">
                    <form action="{{ route('post_kendaraan') }}">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Kode Admin
                                    </th>
                                    <th>
                                        Plat Nomor
                                    </th>
                                    <th>
                                        Jenis Kendaraan
                                    </th>
                                    <th>
                                        Merek
                                    </th>
                                    <th>
                                        Jam Masuk
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($datacari as $row)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$row->id_parkir}}</td>
                                    <td>{{$row->plat_no}}</td>
                                    <td>{{$row->jenis_kendaraan}}</td>
                                    <td>{{$row->merek}}</td>
                                    <td>{{$row->jam_masuk}}</td>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <a href="" class="btn btn-primary">Cetak</a>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection
