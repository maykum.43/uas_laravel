@extends('admin.layouts.master')
@section('content')

@php
date_default_timezone_set("Asia/Jakarta");

$i=1;
$isi = count($data);
$quota = 300;
$cek_sisa = $quota - $isi;


@endphp

<div class="content">
    <div class="row">
        <div class="card">
            <div class="card-body">
                <table>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <tr>
                                    <th>
                                        Parkir Masuk
                                    </th>
                                    <th>
                                        Parkir Keluar
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <form action="{{ route('post_kendaraan') }}" method="post">
                                        @csrf
                                            <div class="row">
                                                <div class="col-md-2 pr-1">
                                                    <div class="form-group">
                                                        <label>ID Admin</label>
                                                        <input type="text" class="form-control" name="id_admin"
                                                            id="id_admin" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 px-1">
                                                    <div class="form-group">
                                                        <label>Plat No.</label>
                                                        <input type="text" class="form-control" name="plat_no"
                                                            id="plat_no" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 px-1">
                                                    <div class="form-group">
                                                        <label>Merek Kendaraan</label>
                                                        <input type="text" class="form-control" name="merek" id="merek"
                                                            value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 pr-2">
                                                    <div class="form-group">
                                                        <label>Jenis Kendaraan</label>
                                                        <br>
                                                        <input type="radio" class="form-radio col-md-1" name="jenken"
                                                            id="Motor" value="Motor">
                                                        Motor
                                                        <input type="radio" class="form-radio col-md-1" name="jenken"
                                                            id="Mobil" value="Mobil">
                                                        Mobil
                                                        <input type="radio" class="form-radio col-md-1" name="jenken"
                                                            id="Lainya" value="Lainya"> Lainya
                                                    </div>
                                                    <Button class="submit btn btn-primary col-md-4" type="submit" style="height: 40px;">Masuk</button>
                                                </div>
                                        </form>
                                    </td>
                                    <td>
                                    <form action="{{ route('search') }}">
                                        <div class="col-md-8 px-1">
                                            <div class="form-group">
                                                <label>ID Parkir</label>
                                                <input type="text" class="form-control" name="cari" id="cari"
                                                    value="">
                                            </div>
                                            <Button class="submit btn btn-primary col-md-12" type="submit" style="height: 40px;">Cek</button>
                                        </div>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    @endsection
