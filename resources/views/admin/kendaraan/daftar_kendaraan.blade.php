@extends('admin.layouts.master')
@section('content')

@php
date_default_timezone_set("Asia/Jakarta");

$i=1;
$isi = count($data);
$quota = 300;
$cek_sisa = $quota - $isi;

@endphp

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Daftar Kendaraan </h4> 
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Kode Parkir
                                    </th>
                                    <th>
                                        Plat Nomor
                                    </th>
                                    <th>
                                        Jenis Kendaraan
                                    </th>
                                    <th>
                                        Merek
                                    </th>
                                    <th>
                                        Jam Masuk
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$row->id_parkir}}</td>
                                    <td>{{$row->plat_no}}</td>
                                    <td>{{$row->jenis_kendaraan}}</td>
                                    <td>{{$row->merek}}</td>
                                    <td>{{$row->jam_masuk}}</td>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <h6 class="title"> Jumlah Kendaraan : {{$isi}} </h6> 
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection
