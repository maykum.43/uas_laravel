@extends('admin.layouts.master')
@section('content')

@php
date_default_timezone_set("Asia/Jakarta");

$i=1;
$isi = count($data);
@endphp

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Daftar Admin</h4>
                    <a href="{{route('create_admin')}}" class="btn btn-primary">Tambah</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        ID Admin
                                    </th>
                                    <th>
                                        Nama
                                    </th>
                                    <th>
                                        No. Telepon
                                    </th>
                                    <th>
                                        Alamat
                                    </th>
                                    <th>
                                        Jenis kelamin
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$row->id_admin}}</td>
                                    <td>{{$row->nama}}</td>
                                    <td>{{$row->no_tlp}}</td>
                                    <td>{{$row->alamat}}</td>
                                    <td>{{$row->jenis_kelamin}}</td>
                                    <td>
                                        <a href="{{route('edit_data', $row->id_admin)}}"
                                            class="btn btn-primary">Edit</a>
                                        <a href="{{route('delete_data', $row->id_admin)}}"
                                            class="btn btn-secondary">Hapus</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
