@extends('admin.layouts.master')
@section('content')

<div class="content">
    <div class="row">
        <div class="col-8 col-md-8 col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>Tambah Data Admin</h4>
                </div>
                <form action="{{ route('post_admin') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Admin</label>
                            <input type="text" class="form-control" name="nama">
                        </div>
                        <div class="form-group">
                            <label>No. Telepon</label>
                            <input type="number" class="form-control" name="no_tlp">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="alamat">
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <input type="text" class="form-control" name="jenis_kelamin">
                        </div>
                        <button class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
