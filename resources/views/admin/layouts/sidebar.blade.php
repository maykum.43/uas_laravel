<div class="sidebar" data-color="orange">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
            CT
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            Creative Tim
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            <li>
                <a href="{{route('tampil_kendaraan')}}">
                    <i class="now-ui-icons design_app"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="{{route('daftar_kendaraan')}}">
                    <i class="now-ui-icons design_bullet-list-67"></i>
                    <p>Daftar Kendaraan</p>
                </a>
                <ul class="nav">
                    <li>
                        <a href="{{route('tampil_admin')}}">
                            <i class="now-ui-icons users_single-02"></i>
                            <p>Admin</p>
                        </a>
                    </li>
            </li>
        </ul>
    </div>
</div>
